# Workshop Docker

This project demonstrate how to build and handle docker images.

## Docker Images

### Ansible
```
docker run -ti -v `pwd`:`pwd` -w `pwd` registry.gitlab.com/frankgiesecke/workshop-docker/ansible sh
```

### Terraform
```
docker run -ti -v `pwd`:`pwd` -w `pwd` registry.gitlab.com/frankgiesecke/workshop-docker/terraform sh
```
